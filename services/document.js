import CodeMirror from 'codemirror';

var lastID = 0;
export const all = [];

export function create(content = '', mode = 'text/x-sql', filePath = null) {
	var doc = new CodeMirror.Doc(content, mode);
	doc.id = ++lastID;
	doc.filePath = filePath;
	return doc;
}

export function destroy(id) {
	delete all[id];
}

export function get(id) {
	return all[id];
}

export function getModeForDriver(driverName) {
	switch(driverName) {
		case 'pg': return 'text/x-pgsql';
		case 'mariasql': return 'text/x-mariadb';
		case 'oracle':
		case 'strong-oracle':
			return 'text/x-plsql';
		case 'mssql': return 'text/x-mssql';
		case 'mysql':
		case 'mysql2':
			return 'text/x-mysql';
		default: return 'text/x-sql';
	}
}

export default {
	all,
	create,
	destroy,
	get,
	getModeForDriver,
};
