import { Menu, app, shell, ipcRenderer } from 'electron';


const menuTemplate = [
	{
		label: 'File',
		submenu: [
			{
				label: 'Open...',
				accelerator: 'CmdOrCtrl+O',
				click() {
					ipcRenderer.send('showOpenDialog');
				}
			},
			{
				label: 'Save As...',
				accelerator: 'Shift+CmdOrCtrl+S',
				click() {
					ipcRenderer.send('showSaveAsDialog');
				}
			},
			{
				label: 'Save',
				accelerator: 'CmdOrCtrl+S',
				click(item, focusedWindow) {
					focusedWindow.webContents.send('saveFile');
				},
			},
			{ type: 'separator' },
			{ label: 'Quit', accelerator: 'CmdOrCtrl+Q', click() { app.quit(); } },
		]
	},
	{
		label: 'Edit',
		submenu: [
			{ label: 'Undo', accelerator: 'CmdOrCtrl+Z', role: 'undo' },
			{ label: 'Redo', accelerator: 'Shift+CmdOrCtrl+Z', role: 'redo' },
			{ type: 'separator' },
			{ label: 'Cut', accelerator: 'CmdOrCtrl+X', role: 'cut' },
			{ label: 'Copy', accelerator: 'CmdOrCtrl+C', role: 'copy' },
			{ label: 'Paste', accelerator: 'CmdOrCtrl+V', role: 'paste' },
			{ label: 'Select All', accelerator: 'CmdOrCtrl+A', role: 'selectall' },
		]
	},
	{
		label: 'View',
		submenu: [
			{
				label: 'Reload',
				//accelerator: 'CmdOrCtrl+R',
				click(item, focusedWindow) {
					if(focusedWindow) { focusedWindow.reload(); }
				}
			},
			{
				label: 'Toggle Full Screen',
				accelerator: process.platform === 'darwin' ? 'Ctrl+Command+F' : 'F11',
				click(item, focusedWindow) {
					if(focusedWindow) { focusedWindow.setFullScreen(!focusedWindow.isFullScreen()); }
				}
			},
			{
				label: 'Toggle Developer Tools',
				accelerator: process.platform === 'darwin' ? 'Alt+Command+I' : 'Ctrl+Shift+I',
				click(item, focusedWindow) {
					if(focusedWindow) { focusedWindow.webContents.toggleDevTools(); }
				}
			},
		]
	},
	{
		label: 'Window',
		role: 'window',
		submenu: [
			{ label: 'Minimize', accelerator: 'CmdOrCtrl+M', role: 'minimize' },
			{ label: 'Close', accelerator: 'CmdOrCtrl+W', role: 'close' },
		]
	},
	{
		label: 'Help',
		role: 'help',
		submenu: [
			{
				label: 'GitLab Project',
				click() { shell.openExternal('https://gitlab.com/whitelynx/querytron'); }
			},
		]
	},
];

if(process.platform === 'darwin') {
	const name = app.getName();

	// File menu.
	menuTemplate[0].submenu.splice(-2, 0,
		{ type: 'separator' },
		{ label: 'About ' + name, role: 'about' },
		{ type: 'separator' },
		{ label: 'Services', role: 'services', submenu: [] },
		{ type: 'separator' },
		{ label: 'Hide ' + name, accelerator: 'Command+H', role: 'hide' },
		{ label: 'Hide Others', accelerator: 'Command+Alt+H', role: 'hideothers' },
		{ label: 'Show All', role: 'unhide' }
	);

	// Window menu.
	menuTemplate[4].submenu.push(
		{ type: 'separator' },
		{ label: 'Bring All to Front', role: 'front' }
	);
}

export const menu = Menu.buildFromTemplate(menuTemplate);
Menu.setApplicationMenu(menu);


export default menu;
