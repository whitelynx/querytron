import mssqlObjects from './mssql';
import pgObjects from './pg';


const dbObjects = {
	mssql: mssqlObjects,
	pg: pgObjects,
};

export default dbObjects;
