import fs from 'fs';
import path from 'path';

import _ from 'lodash';
import Promise from 'bluebird';
import knex from 'knex';
import promiseCacheThrottle from 'promise-cache-throttle';
import pgArray from 'postgres-array';

promiseCacheThrottle(Promise);
Promise.promisifyAll(fs);


const useInformationSchema = false; // Use `information_schema` instead of `pg_catalog` to fetch metadata where possible.
const showUnencryptedPasswords = false;
const grantAllForSinglePrivileges = false; // Show "GRANT ALL PRIVILEGES" when "ALL" corresponds to only one privilege?

let pgObjects;


function loadSQLQuery(filename) {
	return fs.readFileAsync(path.resolve(__dirname, 'pg-queries', filename), 'utf8');
}

function cached(func) {
	func.makeCached = true;
	return func;
}

const dummyKnex = knex({ client: 'pg' });

const reNumber = '\\d+(?:\\.\\d*)|\\.\\d+';
const box2dRE = new RegExp(`BOX\\(\\s*(${reNumber})\\s+(${reNumber}),\\s*(${reNumber})\\s+(${reNumber})\\s*\\)`);
const box3dRE = new RegExp(`BOX3D\\(\\s*(${reNumber})\\s+(${reNumber})(?:\\s+(${reNumber}))?,\\s*(${reNumber})\\s+(${reNumber})(?:\\s+(${reNumber}))?\\s*\\)`);
const aclRE = /^(.*)?=([rwadDxtXUCcT]*)([*])?(?:\/(.*))?$/;

function quote(value) {
	return dummyKnex.raw('?', [value]).toString();
}
function quoteIdentifier(value) {
	if(/^[^a-z_]|[^a-z_0-9$]/.test(value)) {
		return dummyKnex.raw('??', [value]).toString();
	} else {
		return value;
	}
}
function boolOpt(name, enabled) {
	return enabled ? name : `NO${name}`;
}
function renderFlags(object, ...flagKeys) {
	return _.map(flagKeys, key => boolOpt(key.toUpperCase(), object[key]));
}
function renderProps(object, ...propKeys) {
	return _.map(propKeys, key => `${key.toUpperCase()} = ${object[key]}`);
}
function renderQuotedProps(object, ...propKeys) {
	return _.map(propKeys, key => `${key.toUpperCase()} = ${quote(object[key])}`);
}
function renderIdentifierProps(object, ...propKeys) {
	return _.map(propKeys, key => `${key.toUpperCase()} = ${quoteIdentifier(object[key])}`);
}
function addCommentDefinition(object, objectType) {
	if(object.definition && object.description) {
		objectType = objectType || object.type;

		const objectName = object.fullName;

		object.definition += `

COMMENT ON ${objectType.toUpperCase()} ${objectName}
  IS ${quote(object.description)};`;
	}

	return object;
}
function addGrants(object, columnNames, objectType) {
	if(!object.definition) {
		return object;
	}

	objectType = objectType || object.type;

	const objectName = object.fullName;

	columnNames = columnNames || [];
	if(_.isString(columnNames)) { columnNames = [columnNames]; }

	if(object.owner && !columnNames.length) {
		object.definition += `

ALTER ${objectType.toUpperCase()} ${objectName}
  OWNER TO ${quoteIdentifier(object.owner)};`;
	}

	_.forEach(object.acl, aclItem => {
		const match = aclRE.exec(aclItem);
		if(!match) {
			console.warn("Couldn't parse ACL part:", aclItem);
			return;
		}

		let [grantee, privChars, withGrantOption, grantorName] = match.slice(1);
		const sortedPrivChars = _.orderBy(privChars).join('');

		let hasAll = false;
		switch(objectType) {
			case 'table':
			case 'index':
			case 'view':
			case 'materialized view':
			case 'composite type':
			case 'TOAST table':
			case 'foreign table':
				hasAll = (columnNames.length ? sortedPrivChars == 'arwx' : sortedPrivChars == 'Dadrtwx');

				// Regardless of which of these types the object actually is, the GRANT statement needs 'TABLE'.
				objectType = 'table';
				break;

			case 'sequence': hasAll = (sortedPrivChars == 'Urw'); break;
			case 'database': hasAll = (sortedPrivChars == 'CTc'); break;

			case 'large object': hasAll = (sortedPrivChars == 'rw'); break;
			case 'schema': hasAll = (sortedPrivChars == 'CU'); break;

			case 'domain': hasAll = grantAllForSinglePrivileges && (sortedPrivChars == 'U'); break;
			case 'foreign data wrapper': hasAll = grantAllForSinglePrivileges && (sortedPrivChars == 'U'); break;
			case 'foreign server': hasAll = grantAllForSinglePrivileges && (sortedPrivChars == 'U'); break;
			case 'function': hasAll = grantAllForSinglePrivileges && (sortedPrivChars == 'X'); break;
			case 'language': hasAll = grantAllForSinglePrivileges && (sortedPrivChars == 'U'); break;
			case 'tablespace': hasAll = grantAllForSinglePrivileges && (sortedPrivChars == 'C'); break;
			case 'type': hasAll = grantAllForSinglePrivileges && (sortedPrivChars == 'U'); break;
		}

		columnNames = columnNames.length ? ` (${columnNames.join(', ')})` : '';

		let privileges = _(privChars)
			.map(privChar => {
				const privilege = pgObjects.lookups.aclPrivileges[privChar];
				if(privilege) { return privilege.toUpperCase() + columnNames; }
				else { console.warn(`Unrecognized privilege character '${privChar}'!`); }
			})
			.filter()
			.join(', ');

		if(hasAll) {
			privileges = `ALL PRIVILEGES -- ${privileges}`;
		}

		withGrantOption = withGrantOption ? `
  WITH GRANT OPTION` : '';

		object.definition += `

GRANT ${privileges}
  ON ${objectType.toUpperCase()} ${objectName}
  TO ${grantee || 'PUBLIC'}${withGrantOption ? '' : ';'} -- Grantor: ${grantorName}${withGrantOption}`;
	});

	return object;
}


const generateCreateTableStatementQuery = loadSQLQuery('generate_create_table_statement.sql');


function parseGeometry(value) {
	if(value === null) { return null; }
	return wkx.Geometry.parse(new Buffer(value, 'hex'));
}

function parseBox(value) {
	if(value === null) { return null; }

	let bbox, xMin, yMin, xMax, yMax;

	let match = box2dRE.exec(value);
	if(match) {
		bbox = [xMin, yMin, xMax, yMax] = match.slice(1).map(c => parseFloat(c));
	} else if(match = box3dRE.exec(value)) {
		bbox = [xMin, yMin, zMin, xMax, yMax, zMax] = match.slice(1).map(c => parseFloat(c));
		// We ignore Z coords here, just like PostGIS does when casting box3d to geometry.
	} else {
		throw new TypeError("Couldn't parse box value! It should look like: BOX(xmin ymin,xmax ymax), BOX3D(xmin ymin zmin,xmax ymax zmax), or BOX3D(xmin ymin,xmax ymax)");
	}

	return {
		type: 'Polygon',
		bbox,
		coordinates: [[
			[xMin, yMin],
			[xMax, yMin],
			[xMax, yMax],
			[xMin, yMax]
		]]
	};
}

function parseArray(parser) {
	return (value) => {
		if(value === null) { return null; }
		return pgArray.parse(value.replace(/:/g, ','), parser);
	};
}

//FIXME: Why isn't this referenced anywhere? I thought I had this parsing...
const parsers = {
	geometry: parseGeometry,
	geography: parseGeometry,
	box2d: parseBox,
	box3d: parseBox,
	_geometry: parseArray(parseGeometry),
	_geography: parseArray(parseGeometry),
	_box2d: parseArray(parseBox),
	_box3d: parseArray(parseBox)
};


pgObjects = {

	parsers,

	lookups: {
		relpersistence: { p: 'permanent table', u: 'unlogged table', t: 'temporary table' },
		relkind: { r: /*'ordinary table'*/'table', i: 'index', S: 'sequence', v: 'view', m: 'materialized view', c: 'composite type', t: 'TOAST table', f: 'foreign table' },
		relreplident: { d: 'default (primary key, if any)', n: 'nothing', f: 'all columns', i: 'index with indisreplident set, or default' },

		typtype: { b: 'base type', c: 'composite type', d: 'domain', e: 'enum type', p: 'pseudo-type', r: 'range type' },
		typalign: { c: 'char alignment', s: 'short alignment', i: 'int alignment', d: 'double alignment' },
		typstorage: { p: 'must always be stored plain', e: 'can be stored in a "secondary" relation', m: 'can be stored compressed inline', x: 'can be stored compressed inline or stored in "secondary" storage' },

		prokind: { f: /*'normal function'*/'function', p: 'procedure', a: 'aggregate function', w: 'window function' },
		proargmodes: { i: 'in', o: 'out', b: 'inout', v: 'variadic', t: 'table' },
		provolatile: { i: 'immutable', s: 'stable', v: 'volatile' },

		aclPrivileges: { r: 'select', w: 'update', a: 'insert', d: 'delete', D: 'truncate', x: 'references', t: 'trigger', X: 'execute', U: 'usage', C: 'create', c: 'connect', T: 'temporary' },

		contype: { c: 'check constraint', f: 'foreign key constraint', p: 'primary key constraint', u: 'unique constraint', t: 'constraint trigger', x: 'exclusion constraint' },
		confupdtype: { a: 'no action', r: 'restrict', c: 'cascade', n: 'set null', d: 'set default' },
		confdeltype: { a: 'no action', r: 'restrict', c: 'cascade', n: 'set null', d: 'set default' },
		confmatchtype: { f: 'full', p: 'partial', s: 'simple' },
	},


	filterChoices: {
		type: {
			display: 'Type',
			values: {
				schema: 'Schema',

				type: 'User-Defined Type',

				// Relation ("class") types: (from `relkind`)
					table: 'Table',
					index: 'Index',
					sequence: 'Sequence',
					view: 'View',
					'materialized view': 'Materialized view',
					'composite type': 'Composite type',
					'TOAST table': 'TOAST table',
					'foreign table': 'Foreign table',

				// Function/procedure types: (from `prokind`)
				'function': 'Normal function',
				prodcedure: 'Stored procedure',
				'aggregate function': 'Aggregate function',
				'window function': 'Window function',
			},
		},
	},

	queryOptions: null,
	explainOptions: {
		verbose: true,
		costs: true,
		buffers: true,
		timing: true
	},

	settings: {},
	settingDescriptions: {},

	getConnection(connectionConfig) {
		const host = connectionConfig.connection.host;
		[ connectionConfig.connection.host, connectionConfig.connection.port ] = host.split(/:/);

		console.log('Connecting:', connectionConfig);

		const connection = knex(_.merge(connectionConfig, {
			connection: {
				ssl: connectionConfig.connection.requireSSL ? {
					sslmode: 'require',
					rejectUnauthorized: !connectionConfig.connection.allowSelfSignedSSL,
				} : false,
			},
			pool: {
				afterCreate(rawClient, cb) {
					rawClient.on('notice', notice => connection.emit('notice', notice));
					cb(null, rawClient);
				}
			}
		}));
		return connection;
	},

	init(connection) {
		this.connection = connection;
		return this.refresh(connection);
	},

	refresh(connection) {
		return Promise.all([
			this.getTypeIndex(connection),
			this.getSchemaIndex(connection),
			this.getSettings(connection),
		]);
	},

	modifyQuery(query) {
		return query.options({ rowMode: 'array' })
			.on('query', data => {
				console.log("Got 'query' event:", data);
			});
	},

	normalizeRawResponse(response) {
		console.log('normalizeRawResponse(', response, ')');
		return Promise.map(response.fields, (field, idx) => {
			console.log('normalizeRawResponse: looking up type display for field:', field);
			return this._typeDisplay(field.dataTypeID, field.dataTypeSize, field.dataTypeModifier)
				.then(typeDisplay =>
					_.defaults({
						name: field.name,
						type: typeDisplay,
						key: response.rowAsArray ? idx : field.name,
					}, field)
				);
		})
			.then(columns => {
				console.log('normalizeRawResponse: Got processed columns:', columns);
				return {
					rows: response.rows,
					columns,
					affectedRows: response.rowCount,
				};
			});
	},

	withGenerateCreateTableStatementFunctionName: function(connection, callback) {
		return connection.transaction(trx =>
			this.getConnectionState(trx).then(connectionState => {
				const funcname = `"_querytron_${connectionState.sessionBackendPid}_gen_table_def"`;

				return Promise.resolve(
					generateCreateTableStatementQuery.then(query => trx.raw(query.replace('${FUNCNAME}', funcname)))
				)
					.then(() => callback(trx, funcname))
					.finally(() => {
						console.log(`Dropping function ${funcname}...`);
						return trx.raw(`DROP FUNCTION ${funcname}(oid)`);
					});
			})
		);
	},

	getConnectionState: cached(function(connection) {
		return connection.raw(`select
				current_database() as "currentDatabase",
				current_schema() as "currentSchema",
				current_user as "currentUser",
				array_agg(current_schemas(false)::text) as "schemaSearchPath",
				array_agg(current_schemas(true)::text) as "schemaSearchPathWithImplicit",
				(select nspname from pg_catalog.pg_namespace where oid = pg_my_temp_schema()) as "tempSchema",
				pg_backend_pid() as "sessionBackendPid"`
		)
			.then(result => result.rows[0]);
	}),
	getSettings: cached(function(connection) {
		const fetchSettings = connection.raw(`show all`)
			.then((result) => result.rows);

		return Promise.reduce(fetchSettings, (accum, row) => {
				accum.settings[row.name] = row.setting;
				accum.settingDescriptions[row.name] = row.description;
				return accum;
			}, {settings: {}, settingDescriptions: {}})
			.then(results => {
				this.settings = results.settings;
				this.settingDescriptions = results.settingDescriptions;
				return results.settings;
			});
	}),

	getRoles: cached(function(connection) {
		return connection.select(
			'ai.oid as objectID',
			'ai.rolname as name',
			'ai.rolcanlogin as login',
			'ai.rolsuper as superuser'
			//TODO: Finish this!
			)
			.from('pg_catalog.pg_roles as ai')
			.then(roles => this._addCommonProperties(roles, role => role.login ? 'login role' : 'group role'));
	}),
	getRoleIndex: cached(function(connection) {
		return this.getRoles(connection)
			.then(roles => {
				this.roleIndex = _.keyBy(roles, 'objectID');
				return this.roleIndex;
			});
	}),

	getDatabases: cached(function(connection) {
		return this.getRoleIndex(connection)
			.then(() =>
				Promise.map(
					connection.select(
						'd.oid as objectID',
						'd.datname as name',
						'd.datdba as ownerOID'
						)
						.from('pg_catalog.pg_database as d'),
					result => _.defaults(result, {
						tablespace: undefined,
						'connection limit': undefined,
						encoding: undefined,
						lc_collate: undefined,
						lc_ctype: undefined,
						datistemplate: undefined,
						datallowconn: undefined,
						acl: undefined,
					}))
			)
			.then(databases => this._addCommonProperties(databases, 'database'));
	}),

	getSchemas: cached(function(connection) {
		return Promise.try(() => {
			if(useInformationSchema) {
				return connection.select(
					connection.raw('s.schema_name::regnamespace as "objectID"'),
					's.catalog_name as catalogName',
					's.schema_name as name',
					's.schema_owner as owner'
					)
					.from('information_schema.schemata as s');
			} else {
				return connection.select(
					'ns.oid as objectID',
					'ns.nspname as name',
					'ns.nspowner as ownerOID'
					)
					.from('pg_catalog.pg_namespace as ns');
			}
		})
			.map(result => _.defaults(result, {
				acl: undefined,
			}))
			.then(schemas => this._addCommonProperties(schemas, 'schema'));
	}),
	getSchemaIndex: cached(function(connection) {
		return this.getSchemas(connection)
			.then(schemas => {
				this.schemaIndex = _.keyBy(schemas, 'objectID');
				return this.schemaIndex;
			});
	}),

	resolveSchemas: cached(function(connection, objectsPromise) {
		return Promise.join(
			objectsPromise,
			getSchemaIndex(connection),
			(objects, schemaIndex) => _.map(objects, object => _.assign(object, {
				schemaName: schemaIndex[object.schemaOID].name
			}))
		);
	}),

	getTypes: cached(function(connection) {
		return Promise.join(this.getRoleIndex(connection), this.getSchemaIndex(connection), () =>
			Promise.map(
				connection.select(
					't.oid as objectID',
					't.typname as name',
					't.typnamespace as schemaOID',
					't.typowner as ownerOID',
					't.typtype'
					)
					.from('pg_catalog.pg_type as t'),
				result => _.defaults(result, {
					typlen: undefined,
					typbyval: undefined,
					typcategory: undefined,
					typispreferred: undefined,
					typisdefined: undefined,
					typdelim: undefined,
					typrelid: undefined,
					typelem: undefined,
					typarray: undefined,
					typinput: undefined,
					typoutput: undefined,
					typreceive: undefined,
					typsend: undefined,
					typmodin: undefined,
					typmodout: undefined,
					typanalyze: undefined,
					typalign: undefined,
					typstorage: undefined,
					typnotnull: undefined,
					typbasetype: undefined,
					typtypmod: undefined,
					typndims: undefined,
					typcollation: undefined,
					typdefaultbin: undefined,
					typdefault: undefined,
					acl: undefined,
				}))
				.then(types => this._addCommonProperties(types, item => pgObjects.lookups.typtype[item.typtype]))
		);
	}),
	getTypeIndex: cached(function(connection) {
		return this.getTypes(connection)
			.then(types => {
				this.typeIndex = _.keyBy(types, 'objectID');
				return this.typeIndex;
			});
	}),

	getRelations: cached(function(connection) {
		return Promise.join(this.getRoleIndex(connection), this.getSchemaIndex(connection), () =>
			Promise.map(
				connection.select(
					'c.oid as objectID',
					'c.relname as name',
					'c.relnamespace as schemaOID',
					'c.relowner as ownerOID',

					'c.relkind'
					)
					.from('pg_catalog.pg_class as c'),
				result => _.defaults(result, {
					reltype: undefined,
					reloftype: undefined,
					relam: undefined,
					relfilenode: undefined,
					reltablespace: undefined,
					estimatedSizeInPages: undefined,
					estimatedRows: undefined,
					//relallvisible: undefined,
					reltoastrelid: undefined,
					relhasindex: undefined,
					relisshared: undefined,
					relpersistence: undefined,
					relkind: undefined,
					relnatts: undefined,
					relchecks: undefined,
					relhasoids: undefined,
					//relhaspkey: undefined,
					relhasrules: undefined,
					relhastriggers: undefined,
					relhassubclass: undefined,
					relrowsecurity: undefined,
					relforcerowsecurity: undefined,
					relispopulated: undefined,
					relreplident: undefined,
					relfrozenxid: undefined,
					relminmxid: undefined,
					acl: undefined,
					reloptions: undefined,
					constraints: undefined,
				}))
				.then(relations => this._addCommonProperties(relations, item => pgObjects.lookups.relkind[item.relkind]))
		);
	}),
	getRelationIndex: cached(function(connection) {
		return this.getRelations(connection)
			.then(relations => {
				this.relationIndex = _.keyBy(relations, 'objectID');
				return this.relationIndex;
			});
	}),

	getFunctions: cached(function(connection) {
		return Promise.join(this.getRoleIndex(connection), this.getSchemaIndex(connection), () =>
			this.getSettings(connection).then(settings =>
				Promise.map(
					connection.select(
						'p.oid as objectID',
						'p.proname as name',
						'p.pronamespace as schemaOID',
						'p.proowner as ownerOID',

						//'p.pronargs',
						//'p.pronargdefaults',
						//'p.proargnames',
						//'p.proargdefaults',
						'p.proargtypes',
						'p.provariadic',
						'p.proargmodes',
						'p.prorettype',
						'p.proretset as returnsSet',

						settings.server_version_num >= 110000
							? 'p.prokind'
						: connection.raw(
							`case when p.proisagg then 'a'
								when p.proiswindow then 'w'
								else 'f'
							end as prokind`),
						'p.proisstrict',
						'p.provolatile',

						'p.prolang',
						connection.raw('pg_get_function_identity_arguments(p.oid) as "inputArguments"')
					)
					.from('pg_catalog.pg_proc as p'),
				result => _.defaults(result, {
					pronargs: undefined,
					pronargdefaults: undefined,
					proargnames: undefined,
					proargdefaults: undefined,

					procost: undefined,
					prorows: undefined,

					proconfig: undefined,

					acl: undefined,
				}))
				.then(functions => this._addCommonProperties(functions, func => pgObjects.lookups.prokind[func.prokind]))
			)
		);
	}),
	getFunctionIndex: cached(function(connection) {
		return this.getFunctions(connection)
			.then(functions => {
				this.functionIndex = _.keyBy(functions, 'objectID');
				return this.functionIndex;
			});
	}),

	getObjects: cached(function(connection) {
		return Promise.join(
			this.getRoles(connection),
			this.getDatabases(connection),
			this.getSchemas(connection),
			this.getTypes(connection),
			this.getRelations(connection),
			this.getFunctions(connection),

			(roles, databases, schemas, types, relations, functions) =>
			{
				return _.uniqBy(_.concat(roles, databases, schemas, types, relations, functions), 'id');
			}
		)
			.catch(exc => {
				console.error("Error getting DB objects:", exc);
				throw exc;
			});
	}),

	_getIcon(type) {
		switch(type) {
			case 'database': return 'uk-icon-database';

			case 'login role': return 'uk-icon-user';
			case 'group role': return 'uk-icon-group';

			case 'schema': return 'uk-icon-list-alt';

			case 'base type': return 'uk-icon-tag'; //TODO: Better icon?
			//case 'composite type': return 'uk-icon-file-zip-o';
			case 'domain': return 'uk-icon-tag'; //TODO: Better icon?
			case 'enum type': return 'uk-icon-tag'; //TODO: Better icon?
			case 'pseudo-type': return 'uk-icon-tag'; //TODO: Better icon?
			case 'range type': return 'uk-icon-tag'; //TODO: Better icon?

			case 'function': return 'uk-icon-long-arrow-right';
			case 'procedure': return 'uk-icon-long-arrow-down'; //TODO: Better icon?
			case 'aggregate function': return 'uk-icon-long-arrow-right'; //TODO: Better icon?
			case 'window function': return 'uk-icon-long-arrow-right'; //TODO: Better icon?

			case 'table': return 'uk-icon-table';
			case 'index': return 'uk-icon-indent';
			case 'sequence': return 'uk-icon-list-ol';
			case 'view': return 'uk-icon-search';
			case 'materialized view': return 'uk-icon-search-plus';
			case 'composite type': return 'uk-icon-file-zip-o';
			case 'TOAST table': return 'uk-icon-th-large';
			case 'foreign table': return 'uk-icon-caret-square-o-right';

			default: return 'uk-icon-cube';
		}
	},

	_typeDisplay: cached(function(typeOID, typeSize, typeModifier) {
		return this.connection.select(this.connection.raw('format_type(?, ?)', [typeOID, typeModifier]))
			.then(result => result[0].format_type);
	}),

	_getDisplay(schema, item, type) {
		let icon = this._getIcon(type);
		icon = `<i class="${icon}" title="${type}"></i>`;

		let display;
		if(schema) {
			display = `${icon} ${schema.name}.<span class="uk-text-primary">${item.name}</span>`;
		} else {
			display = `${icon} <span class="uk-text-primary">${item.name}</span>`;
		}

		switch(type) {
			case 'function':
				return display + `(<span class="uk-text-muted">${item.inputArguments}</span>)`;

			default:
				return display;
		}
	},

	_addCommonProperties(items, type) {
		return _.map(items, item => {
			const itemType = _.isFunction(type) ? type(item) : type;

			_.defaults(item, {
				definition: undefined,
			});

			const schema = this.schemaIndex && this.schemaIndex[item.schemaOID];
			if(schema) {
				item = _.assign(
					{ fullName: `${schema.name}.${item.name}`, type: itemType, description: item.description },
					item,
					{ schemaName: schema.name, id: `${schema.name}.${item.name}` }
				);
			} else {
				_.assign(
					{ fullName: item.name, type: itemType, description: item.description },
					item,
					{ id: item.name }
				);
			}

			if(item.inputArguments) {
				item.fullName += `(${item.inputArguments})`;
				item.id += `(${item.inputArguments})`;
			}

			item.id += `:${itemType}`;

			const owner = this.roleIndex && this.roleIndex[item.ownerOID] && this.roleIndex[item.ownerOID].name;
			if(owner) {
				_.assign(item, { owner });
			}

			Object.defineProperty(item, 'display', { enumerable: false, value: this._getDisplay(schema, item, itemType) });

			return item;
		});
	},

	getColumnInfo: cached(function(connection, relation) {
		return connection(relation).columnInfo();
	}),

	getDetails: cached(function(connection, object) {
		switch(object.type) {
			case 'database': return this.getDatabaseDetails(connection, object);

			case 'login role':
			case 'group role':
				return this.getRoleDetails(connection, object);

			case 'schema': return this.getSchemaDetails(connection, object);

			case 'composite type': // A special case -- this counts as both a type _and_ a relation.
				return this.getTypeDetails(connection, object)
					.then(() => this.getRelationDetails(connection, object));

			case 'base type':
			case 'domain':
			case 'enum type':
			case 'pseudo-type':
			case 'range type':
				return this.getTypeDetails(connection, object);

			case 'function': return this.getFunctionDetails(connection, object);

			case 'table':
			case 'index':
			case 'sequence':
			case 'view':
			case 'materialized view':
			case 'TOAST table':
			case 'foreign table':
				return this.getRelationDetails(connection, object);

			default: return Promise.resolve(object);
		}
	}),

	getDatabaseDetails: cached(function(connection, database) {
		return connection.select(
			connection.raw('obj_description(db.oid, ?) as description', ['pg_database']),
			'db.dattablespace as tablespace',
			'db.datconnlimit as connection limit',
			'db.encoding as encoding',
			'db.datcollate as lc_collate',
			'db.datctype as lc_ctype',
			'db.datistemplate',
			'db.datallowconn',
			connection.raw('db.datacl::text[] as acl')
		)
			.from('pg_catalog.pg_database as db')
			.where('db.oid', database.objectID)
			.then(results => {
				_.assign(database, results[0]);

				const props = _.concat(
					renderIdentifierProps(database, 'owner', 'tablespace'),
					database['connection limit'] !== -1 ? renderProps(database, 'connection limit') : [],
					renderQuotedProps(database, 'encoding', 'lc_collate', 'lc_ctype')
				);

				database.definition = `CREATE DATABASE ${quoteIdentifier(database.name)}
	WITH ${props.join('\n\t\t')};`;

				addCommentDefinition(database);
				addGrants(database);

				return database;
			});
	}),

	getRoleDetails: cached(function(connection, role) {
		return Promise.join(
			// Get secondary properties of this role.
			connection.select(
				connection.raw('shobj_description(ai.oid, ?) as description', ['pg_authid']),
				'ai.rolsuper as superuser',
				'ai.rolinherit as inherit',
				'ai.rolcreatedb as createdb',
				'ai.rolcreaterole as createrole',
				'ai.rolreplication as replication',
				'ai.rolconnlimit as connection limit',
				'ai.rolvaliduntil as valid until',
				'ai.rolcatupdate',
				connection.raw('ai.rolpassword is not null as "hasPassword"'),
				connection.raw('ai.rolpassword like ? as "passwordIsEncrypted"', ['md5%']),
				connection.raw(
					`case when ai.rolpassword is not null and (ai.rolpassword like ? or ?)
						then ai.rolpassword
						else null
					end as password`,
					['md5%', showUnencryptedPasswords]
				)
			)
				.from('pg_catalog.pg_authid as ai')
				.where('ai.oid', role.objectID)
				.then(results => results[0]),

			// Get roles that this role is a member of.
			connection.select(
				'am.roleid',
				'am.grantor',
				'am.admin_option'
			)
				.from('pg_catalog.pg_auth_members as am')
				.where('am.member', role.objectID),

			// Get roles that are members of this role.
			connection.select(
				'am.member',
				'am.grantor',
				'am.admin_option'
			)
				.from('pg_catalog.pg_auth_members as am')
				.where('am.roleid', role.objectID),

			(properties, groups, members) => {
				_.assign(role, properties);

				let passwordDef;
				if(role.password) {
					passwordDef = `${role.passwordIsEncrypted ? 'ENCRYPTED ' : ''}PASSWORD ${quote(role.password)}`;
				} else if(role.hasPassword) {
					passwordDef = '-- Has unencrypted password! (hidden)';
				}

				const flags = renderFlags(role, 'login', 'superuser', 'inherit', 'createdb', 'createrole', 'replication');
				const props = _.concat(
					role['connection limit'] !== -1 ? renderProps(role, 'connection limit') : [],
					role['valid until'] !== null ? renderQuotedProps(role, 'valid until') : []
				);

				role.definition = [`CREATE ROLE ${quoteIdentifier(role.name)}`].concat(flags, passwordDef || [], props).join('\n\t') + ';';

				if(role.rolcatupdate) {
					role.definition += `
	-- Role is allowed to update system catalogs directly.
`;
				}

				role.memberOf = _.map(groups, group => {
					let groupName = this.roleIndex[group.roleid].name;
					const grantorName = this.roleIndex[group.grantor].name;
					role.definition += `
GRANT ${quoteIdentifier(groupName)} TO ${quoteIdentifier(role.name)}${group.admin_option ? ' WITH ADMIN OPTION' : ''}; -- Granted by ${grantorName}`;
					if(group.admin_option) {
						groupName += ' (with admin)';
					}
					return groupName;
				});

				role.members = _.map(members, member => {
					let memberName = this.roleIndex[member.member];
					if(member.admin_option) {
						memberName += ' (admin)';
					}
					return memberName;
				});

				addCommentDefinition(role, 'role');

				return role;
			}
		);
	}),

	getSchemaDetails: cached(function(connection, schema) {
		schema.definition = `CREATE SCHEMA IF NOT EXISTS ${quoteIdentifier(schema.name)}
	AUTHORIZATION ${quoteIdentifier(schema.owner)};`;

		return connection.select(
			connection.raw('obj_description(ns.oid, ?) as description', ['pg_namespace']),
			connection.raw('ns.nspacl::text[] as acl')
			)
			.from('pg_catalog.pg_namespace as ns')
			.where('ns.oid', schema.objectID)
			.then(results => _.assign(schema, results[0]))
			.then(results => {
				addCommentDefinition(schema);
				return results;
			})
			.then(results => {
				addGrants(schema);
				return results;
			});
	}),

	getTypeDetails: cached(function(connection, type) {
		return connection.select(
			connection.raw('obj_description(t.oid, ?) as description', ['pg_type']),

			't.typlen',
			't.typbyval',
			't.typcategory',
			't.typispreferred',
			't.typisdefined',
			't.typdelim',
			't.typrelid',
			't.typelem',
			't.typarray',
			't.typinput',
			't.typoutput',
			't.typreceive',
			't.typsend',
			't.typmodin',
			't.typmodout',
			't.typanalyze',
			't.typalign',
			't.typstorage',
			't.typnotnull',
			't.typbasetype',
			't.typtypmod',
			't.typndims',
			't.typcollation',
			't.typdefaultbin',
			't.typdefault',
			connection.raw('t.typacl::text[] as acl')
			)
			.from('pg_catalog.pg_type as t')
			.where('oid', type.objectID)
			.then(results => _.assign(type, results[0]))
			.then(results => {
				addCommentDefinition(type);
				return results;
			})
			.then(results => {
				addGrants(type);
				return results;
			});
	}),

	getRelationDetails: cached(function(connection, relation) {
		return this.getSettings(connection).then(settings =>
			this.withGenerateCreateTableStatementFunctionName(connection, (trx, funcname) =>
				trx.select(
					trx.raw('obj_description(c.oid, ?) as description', ['pg_class']),

					'c.reltype',
					'c.reloftype',
					'c.relam',
					'c.relfilenode',
					'c.reltablespace',
					'c.relpages as estimatedSizeInPages',
					'c.reltuples as estimatedRows',
					//'c.relallvisible',
					'c.reltoastrelid',
					'c.relhasindex',
					'c.relisshared',
					'c.relpersistence',
					'c.relkind',
					'c.relnatts',
					'c.relchecks',
					'c.relhasoids',
					//'c.relhaspkey',
					'c.relhasrules',
					'c.relhastriggers',
					'c.relhassubclass',
					//'c.relrowsecurity',
					//'c.relforcerowsecurity',
					'c.relispopulated',
					'c.relreplident',
					'c.relfrozenxid',
					'c.relminmxid',
					trx.raw('c.relacl::text[] as acl'),
					'c.reloptions',
					trx.raw(`case c.relkind
						when 'v' then
							format('CREATE VIEW %I.%I AS\n %s',
								(select ns.nspname as name from pg_catalog.pg_namespace as ns where ns.oid = c.relnamespace),
								c.relname,
								pg_get_viewdef(c.oid, true)
							)
						when 'r' then
							${funcname}(c.oid)
						when 'i' then
							pg_catalog.pg_get_indexdef(c.oid) || ';'
						else NULL
					end as definition`),
					trx
						.select(trx.raw(`json_agg(row_to_json(sub))`))
						.from(trx
							.select(
								'cn.conname as name',
								trx.raw('pg_get_constraintdef(cn.oid, true) as definition'),
								'cn.oid as objectID',
								'cn.connamespace as schemaOID',
								'cn.contype',
								'cn.condeferrable',
								'cn.condeferred',
								'cn.convalidated',
								'cn.confrelid',
								'cn.confupdtype',
								'cn.confdeltype',
								'cn.confmatchtype',
								'cn.conislocal',
								'cn.coninhcount',
								'cn.connoinherit',
								'cn.conkey as constrainedColumns',
								'cn.confkey as referencedColumns'
								//'cn.conpfeqop',
								//'cn.conppeqop',
								//'cn.conffeqop',
								//'cn.conexclop'
							)
							.from('pg_catalog.pg_constraint as cn')
							.where('cn.conrelid', trx.raw('c.oid'))
							.as('sub')
						)
						.as('constraints')
					)
					.from('pg_catalog.pg_class as c')
					.where('oid', relation.objectID)
			)
				.then(results => {
					relation.estimatedSizeInBytes = relation.estimatedSizeInPages * parseInt(settings.block_size, 10);

					if(results[0]) {
						results[0].constraints = this._addCommonProperties(results[0].constraints, 'constraint');
					}

					return _.assign(relation, results[0]);
				})
				.then(results => {
					addCommentDefinition(relation);
					return results;
				})
				.then(results => {
					addGrants(relation);
					return results;
				})
		);
	}),

	getFunctionDetails: cached(function(connection, func) {
		return this.getSettings(connection)
			.then(settings =>
				connection.select(
					connection.raw('obj_description(p.oid, ?) as description', ['pg_proc']),

					'p.pronargs',
					'p.pronargdefaults',
					'p.proargnames',
					'p.proargdefaults',

					'p.procost',
					'p.prorows',

					//'p.prosrc',
					settings.server_version_num >= 110000
						? connection.raw("case when p.prokind = 'a' then null else pg_get_functiondef(p.oid) end as definition")
						: connection.raw('case when p.proisagg then null else pg_get_functiondef(p.oid) end as definition'),
					'p.proconfig',

					connection.raw('p.proacl::text[] as acl')
				)
				.from('pg_catalog.pg_proc as p')
				.where('oid', func.objectID)
			)
			.then(results => {
				_.assign(func, results[0]);
				if(func.definition) {
					func.definition = func.definition.replace(/[\n\s]*$/, ';');
				}
				return func;
			})
			.then(results => {
				addCommentDefinition(func);
				return results;
			})
			.then(results => {
				addGrants(func);
				return results;
			});
	}),

};

Promise.cachifyAll(pgObjects, { suffix: '', filter(_name, func) { return func.makeCached; } });

export default pgObjects;
