import _ from 'lodash';
import Promise from 'bluebird';
import promiseCacheThrottle from 'promise-cache-throttle';

promiseCacheThrottle(Promise);


function cached(func) {
	func.makeCached = true;
	return func;
}


const mssqlObjects = {

	queryOptions: {
		includeActualExecutionPlan: false,
	},
	explainOptions: {
		verbose: true,
	},
	
	init(connection) {
		return this.refresh(connection);
	},

	refresh(connection) {
		return Promise.all([
			//this.getTypeIndex(connection),
			//this.getSchemaIndex(connection),
		]);
	},

	normalizeRawResponse(response) {
		if(!response) {
			return;
		}

		var responseTable = response.toTable();
		return {
			rows: responseTable.rows,
			columns: _.map(responseTable.columns, (column, idx) =>
				_.defaults({
					name: column.name,
					type: column.type && column.type.declaration,
					key: idx,
				}, column)
			),
		};
	},

};

Promise.cachifyAll(mssqlObjects, { suffix: '', filter(name, func) { return func.makeCached; } });

export default mssqlObjects;
