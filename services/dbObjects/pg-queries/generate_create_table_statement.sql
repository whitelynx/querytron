-- From http://stackoverflow.com/a/16154183/677694, modified:
CREATE OR REPLACE FUNCTION ${FUNCNAME}(p_table_oid oid)
  RETURNS text AS
$BODY$
DECLARE
    v_table_ddl   text;
    table_info record;
    column_record record;
BEGIN
    SELECT
        n.nspname as schema_name,
        c.relname as table_name,
        c.reloptions as table_options
	INTO table_info
    FROM pg_catalog.pg_class c
        LEFT JOIN pg_catalog.pg_namespace n ON n.oid = c.relnamespace
    WHERE c.oid = p_table_oid;

    FOR column_record IN
        SELECT
            a.attname as column_name,
            pg_catalog.format_type(a.atttypid, a.atttypmod) as column_type,
            CASE WHEN
                (SELECT substring(pg_catalog.pg_get_expr(d.adbin, d.adrelid) for 128)
                 FROM pg_catalog.pg_attrdef d
                 WHERE d.adrelid = a.attrelid AND d.adnum = a.attnum AND a.atthasdef) IS NOT NULL THEN
                ' DEFAULT '|| (SELECT substring(pg_catalog.pg_get_expr(d.adbin, d.adrelid) for 128)
                              FROM pg_catalog.pg_attrdef d
                              WHERE d.adrelid = a.attrelid AND d.adnum = a.attnum AND a.atthasdef)
            ELSE
                ''
            END as column_default_value,
            CASE WHEN a.attnotnull = true THEN
                ' NOT NULL'
            ELSE
                ' NULL'
            END as column_not_null,
            a.attnum as attnum,
            e.max_attnum as max_attnum
        FROM
            pg_catalog.pg_attribute a
            INNER JOIN
             (SELECT
                  a.attrelid,
                  max(a.attnum) as max_attnum
              FROM pg_catalog.pg_attribute a
              WHERE a.attnum > 0
                AND NOT a.attisdropped
              GROUP BY a.attrelid) e
            ON a.attrelid=e.attrelid
        WHERE a.attnum > 0
          AND NOT a.attisdropped
          AND a.attrelid = p_table_oid
        ORDER BY a.attnum
    LOOP
        IF column_record.attnum = 1 THEN
            v_table_ddl:='CREATE TABLE '||table_info.schema_name||'.'||table_info.table_name||' (';
        ELSE
            v_table_ddl:=v_table_ddl||',';
        END IF;

        IF column_record.attnum <= column_record.max_attnum THEN
            v_table_ddl:=v_table_ddl||chr(10)||
                     '    '||column_record.column_name||' '||column_record.column_type||column_record.column_default_value||column_record.column_not_null;
        END IF;
    END LOOP;

    v_table_ddl:=v_table_ddl||chr(10)||')';

    IF array_length(table_info.table_options, 1) > 0 THEN
            v_table_ddl:=v_table_ddl||chr(10)||'WITH ('||chr(10)||array_to_string(table_info.table_options, chr(10)||',')||chr(10)||')';
    END IF;

    v_table_ddl:=v_table_ddl||';';
    RETURN v_table_ddl;
END;
$BODY$
  LANGUAGE 'plpgsql' COST 100.0 SECURITY INVOKER;
