var queryParamRE = /\$(\d+)/g;
var commentOrStringRE = /--.*\n|\/\*.*?(?:\n.*?)*?\*\/|\$([a-zA-Z_]\w*)?\$(?:\n.*?)*?\$\1\$|(['"]).*?\2/g;
var statementSeparatorRE = /\s*;+\s*/g;
var whitespaceOrCommentOnlyRE = /^\s*(?:(?:--.*$|\/\*.*?(?:\n.*?)*?\*\/)\s*)*$/g;


function getActiveQueries(activeText, options)
{
    var startIndex = options.startIndex;

    var queryTextParts = [], referencedParams = [];

    function processParamsIn(substr)
    {
        return substr.replace(queryParamRE, function(match, paramIdx)
        {
            paramIdx = parseInt(paramIdx, 10);

            var resultingIdx = referencedParams.indexOf(paramIdx) + 1;
            if(resultingIdx === 0)
            {
                resultingIdx = referencedParams.push(paramIdx);
            } // end if

            //return '$' + resultingIdx; // For node-postgres
            return '?'; // For knex
        });
    } // end processParamsIn

    var queries = [];
    var lastStmtEnd = 0, lastMatchEnd = 0, match;

    function pushQuery()
    {
        // We just reached the end of a statement; add it to the queries array, and clear our buffers.
        var queryText = queryTextParts.join('');
        if(whitespaceOrCommentOnlyRE.test(queryText))
        {
            // This query chunk only contains whitespace; skip it.
            return;
        } // end if

        queries.push({
            text: queryText,
            values: referencedParams.map(function(paramIdx)
            {
                return options.queryParams[paramIdx - 1];
            }),
            startIndex: startIndex
        });

        queryTextParts = [];
        referencedParams = [];
        startIndex = options.startIndex + lastStmtEnd;
    } // end pushQuery

    function processNextQueryChunk(chunk)
    {
        chunk = processParamsIn(chunk);

        var lastStmtSepEnd = 0, stmtSepMatch;
        while((stmtSepMatch = statementSeparatorRE.exec(chunk)) !== null)
        {
            queryTextParts.push(chunk.slice(lastStmtSepEnd, stmtSepMatch.index));

            // Track the end index of the last statement separator in the current chunk.
            lastStmtSepEnd = statementSeparatorRE.lastIndex;

            // Track the end index of the last statement in the current active text.
            lastStmtEnd = lastMatchEnd + lastStmtSepEnd;

            // Push the query that we just finished processing.
            pushQuery();
        } // end while

        queryTextParts.push(chunk.slice(lastStmtSepEnd));
    } // end processNextQueryChunk

    while((match = commentOrStringRE.exec(activeText)) !== null)
    {
        // Process the next chunk of query text.
        processNextQueryChunk(activeText.slice(lastMatchEnd, match.index));

        // Just push the comment or string, without processing its contents.
        queryTextParts.push(match[0]);

        // Track the end of the last match.
        lastMatchEnd = commentOrStringRE.lastIndex;
    } // end while

    // Process the last chunk of query text.
    processNextQueryChunk(activeText.slice(lastMatchEnd));
    pushQuery();

    return queries;
} // end getActiveQueries

export default getActiveQueries;
