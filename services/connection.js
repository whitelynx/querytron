import fs from 'fs';
import path from 'path';

import _ from 'lodash';
import Promise from 'bluebird';
import knex from 'knex';
import Vue from 'vue';

import dbObjects from './dbObjects';
import { config, save as saveConfig } from './config';


const connectionService = {
    currentConnection: null,
    currentConnectionConfig: null,
    currentConnectionName: null,
	lastError: null,
	lastErrorConnectionName: null,

    drivers: _(fs.readdirSync(path.join(path.dirname(require.resolve('knex/lib/')), 'dialects')))
        .filter((dialect) => dialect != 'websql')
        .map((dialect) => [dialect, require('knex/lib/dialects/' + dialect).prototype.driverName])
        .fromPairs()
        .value(),

    connectionConfigs: config.connections,

    defaultConnectionConfig: {
        pool: { min: 1, max: 2 }
    },

    setConnectionConfig(connName, connConfig) {
        if(connName === null) {
            return Promise.reject(new Error("setConnectionConfig(): No connection config name set!"));
        }
        if(connName == this.currentConnectionName) {
            return this.disconnect()
                .then(() => this.setConnectionConfig(connName, connConfig))
                .then(() => this.connect(connName));
        } // end if

        Vue.set(this.connectionConfigs, connName, connConfig);

        return saveConfig();
    },

    deleteConnectionConfig(connName) {
        if(connName === null) {
            return Promise.reject(new Error("deleteConnectionConfig(): No connection config name given!"));
        }
        if(connName == this.currentConnectionName) {
            return this.disconnect()
                .then(() => this.deleteConnectionConfig(connName));
        } // end if

        Vue.delete(this.connectionConfigs, connName);

        return saveConfig();
    },

    connect(connName) {
        if(this.currentConnection)
        {
            return this.disconnect()
                .then(() => this.connect(connName));
        } // end if

		this.currentConnection = null;
		this.currentConnectionName = connName;
        this.currentConnectionConfig = _.assign({}, this.defaultConnectionConfig, this.connectionConfigs[connName]);

		switch(this.currentConnectionConfig.client) {
			case 'mssql':
				this.currentConnectionConfig.connection.requestTimeout = 600000;
				break;
		}

		var dbObj = dbObjects[this.currentConnectionConfig.client];
		var connection;
		if(dbObj.getConnection) {
			connection = dbObj.getConnection(this.currentConnectionConfig);
		} else {
			connection = knex(this.currentConnectionConfig);
		}
		connection.dbObjects = dbObj;
		this.lastErrorConnectionName = null;
		this.lastError = null;

		connection.on('notice', function(notice)
		{
			console.log("NOTICE from PostgreSQL:", notice);
		}); // end 'notice' handler

		var poolError;
		connection.client.pool.on('error', function(error)
		{
			console.error("Error from knex client pool:", error);
			poolError = error;
		}); // end 'error' handler

		console.log("Connecting to %s...", connName);
		return Promise.join(
			connection.dbObjects ? connection.dbObjects.init(connection) : '',
			connection.select(1),
			() => {
				this.currentConnection = connection;
				console.log("Connected to %s.", connName);
			}
		)
			.catch(exc => {
				if(exc.message == 'Pool was destroyed' && poolError) {
					exc = poolError;
					console.error("Pool error connecting to %s:", connName, exc);
				} else {
					console.error("Error connecting to %s:", connName, exc);
				}

				this.lastError = exc;
				this.lastErrorConnectionName = connName;

				this.currentConnectionName = null;
				this.currentConnectionConfig = null;

				throw exc;
			});
    },

    disconnect() {
        if(!this.currentConnection)
        {
            return Promise.resolve();
        } // end if

        return this.currentConnection.destroy()
            .then(() => {
                this.currentConnection = null;
                this.currentConnectionConfig = null;
                this.currentConnectionName = null;
            });
    },
};

export default connectionService;
