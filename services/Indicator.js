import Promise from 'bluebird';

export default class Indicator {
    constructor() {
        this.active = false;
    }

    /**
     * Activate this indicator, start the given process, and deactivate the indicator once the process has finished.
     *
     * @param {function():Promise} processCB - a function defining the process to run and returning a promise for when
     *          that process has finished
     *
     * @return {Promise} the promise returned by `processCB`
     */
    indicate(processCB) {
        this.active = true;

        return Promise.try(processCB)
            .finally(() => this.active = false);
    }
}
