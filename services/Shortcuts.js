import util from 'util';

import _ from 'lodash';
import debug from 'debug';
import Vue from 'vue';

import { config } from './config';


const keyEventCodeRE = /^(?:Key|Digit)?(.*)$/;
const debugLog = debug('Shortcuts');

/**
 * Key identifiers (as represented by
 * [KeyboardEvent.keyIdentifier](https://developer.mozilla.org/en-US/docs/Web/API/KeyboardEvent/keyIdentifier)) used in
 * place of [KeyboardEvent.code](https://developer.mozilla.org/en-US/docs/Web/API/KeyboardEvent/code) for certain keys.
 *
 * @var Shortcuts~keyIdentifiersToUse
 * @type {string[]}
 */
const keyIdentifiersToUse = ['Control', 'Alt', 'Shift', 'Win'];

const abbreviate = true;
const modifiersAsSymbols = false;
const winOnOtherOS = true;


/**
 * An object containing all registered actions and their descriptions, grouped by context.
 *
 * @type {Object.<string,Object.<string,string>>}
 */
export const contextActions = {};


/**
 * An `Error` subclass that is thrown when adding a new shortcut that uses the same key combination as another
 * configured shortcut in the same context.
 *
 * @constructor
 */
/*
class DuplicateShortcut extends Error {
	constructor(newShortcut, existingShortcut) {
		super();
		this.message = 'Another shortcut already uses this key combination in this context.';
		this.name = 'DuplicateShortcut';
		this.newShortcut = newShortcut;
		this.existingShortcut = existingShortcut;
		Error.captureStackTrace(this, DuplicateShortcut);
	}
}
 /*/
// Until ES6-style classes actually _work_ for bluebird's `.catch()`...
function DuplicateShortcut(newShortcut, existingShortcut) {
	this.message = 'Another shortcut already uses this key combination in this context.';
	this.name = 'DuplicateShortcut';
	this.newShortcut = newShortcut;
	this.existingShortcut = existingShortcut;
	Error.captureStackTrace(this, DuplicateShortcut);
}
DuplicateShortcut.prototype = Object.create(Error.prototype);
DuplicateShortcut.prototype.constructor = DuplicateShortcut;
//*/

/**
 * Retrieve a list of registered shortcut contexts.
 *
 * @returns {string[]} the names of each registered context
 */
export function getContexts() {
	return _.keys(contextActions);
}


/**
 * Retrieve available actions for a context.
 *
 * @returns {string[]} the names and descriptions of each registered action in the given context
 */
export function getActions(context) {
	return contextActions[context] || {};
}


/**
 * Retrieve configured shortcuts for a context.
 *
 * @param {string} context - the name of the context to look up
 *
 * @returns {Shortcuts~Config} the configured shortcuts in the given context
 */
export function getContextShortcuts(context) {
	return (config.shortcuts || {})[context] || {};
}


/**
 * Add a new configured shortcut to a context.
 *
 * @param {string} context - the name of the context to add the shortcut to
 * @param {Shortcuts~Definition} shortcutDef - the new shortcut to add
 */
export function addShortcut(context, shortcutDef) {
	let contextShortcuts = config.shortcuts[context];
	if(!contextShortcuts) {
		contextShortcuts = {};
		Vue.set(config.shortcuts, context, contextShortcuts);
	}

	let contextKeyShortcuts = contextShortcuts[shortcutDef.key];
	if(!contextKeyShortcuts) {
		contextKeyShortcuts = [];
		Vue.set(contextShortcuts, shortcutDef.key, contextKeyShortcuts);
	}

	const duplicateShortcutDef = findDuplicateShortcut(context, shortcutDef);
	if(duplicateShortcutDef) {
		throw new DuplicateShortcut(shortcutDef, duplicateShortcutDef);
	}

	contextKeyShortcuts.push(shortcutDef);
}


/**
 * Remove a configured shortcut from a context.
 *
 * @param {string} context - the name of the context to remove the shortcut from
 * @param {Shortcuts~Definition} shortcutDef - the shortcut to remove
 */
export function removeShortcut(context, shortcutDef) {
	const contextShortcuts = config.shortcuts[context];
	if(!contextShortcuts) {
		console.warn('removeShortcut(', context, ',', shortcutDef, `): Context ${JSON.stringify(context)} not found!`);
		return;
	}

	const contextKeyShortcuts = contextShortcuts[shortcutDef.key];
	if(!contextKeyShortcuts) {
		console.warn('removeShortcut(', context, ',', shortcutDef, `): Key ${JSON.stringify(shortcutDef.key)} not set in context ${JSON.stringify(context)}!`);
		return;
	}

	contextKeyShortcuts.$remove(shortcutDef);
}


/**
 * Find a configured shortcut in the given context that matches the same key combination as the given shortcut
 * definition.
 *
 * @param {string} context - the name of the context to check for duplicates in
 * @param {Shortcuts~Definition} shortcutDef - the shortcut definition to compare
 *
 * @returns {?Shortcuts~Definition} if a configured shortcut was found that matches the same key combination, that shortcut
 * 			definition is returned; otherwise, `null`
 */
export function findDuplicateShortcut(context, shortcutDef) {
	const contextShortcuts = config.shortcuts[context] || {};
	const contextKeyShortcuts = contextShortcuts[shortcutDef.key] || [];

	return _.find(contextKeyShortcuts, otherDef => shortcutsUseSameKeyCombo(shortcutDef, otherDef)) || null;
}


/**
 * Compare two shortcut definitions to see if they use the same key combination.
 *
 * @param {Shortcuts~Definition} shortcutDefA - the first shortcut definition to compare
 * @param {Shortcuts~Definition} shortcutDefB - the second shortcut definition to compare
 *
 * @returns {boolean} `true` if both shortcut definitions use the same key combination; `false` otherwise
 */
export function shortcutsUseSameKeyCombo(shortcutDefA, shortcutDefB) {
	return (shortcutDefA.key == shortcutDefB.key) &&
		(shortcutDefA.ctrl == shortcutDefB.ctrl) &&
		(shortcutDefA.alt == shortcutDefB.alt) &&
		(shortcutDefA.shift == shortcutDefB.shift) &&
		(shortcutDefA.meta == shortcutDefB.meta);
}


/**
 * Register available actions for a context.
 *
 * @param {string} context - the name of the context of this set of actions; used to look up available actions when
 * 			configuring, and to get and store shortcut configuration
 * @param {(Array.<string>|Object.<string,string>)} actions - either an array of available action names, or an object
 * 			whose keys represent available action names, and whose values contain descriptions of those actions
 */
export function registerActions(context, actions) {
	if(_.isArray(actions)) {
		actions = _.zipObject(actions, actions);
	}

	Vue.set(contextActions, context, _.defaults(actions, contextActions[context]));
}


/**
 * Generate a readable string representing the given shortcut definition's bound action.
 *
 * @param {Shortcuts~Definition} shortcut - the shortcut definition to display
 *
 * @returns {string} - the human-readable representation of the given shortcut definition's action
 */
export function displayShortcutAction(shortcutDef) {
	if(shortcutDef.args) {
		const argsDisplay = _.map(shortcutDef.args, arg => util.inspect(arg)).join(', ');
		return `${shortcutDef.action}(${argsDisplay})`;
	} else {
		return `${shortcutDef.action}`;
	}
}


/**
 * Get the displayed name for a given keyboard event modifier property.
 *
 * @param {string} modKey - the keyboard event modifier property name to display (`ctrl`, `alt`, `shift`, `meta`)
 * @returns {string} the (platform-specific) string to be displayed
 */
export function displayModKey(modKey) {
	switch(modKey) {
		case 'ctrl':
			return abbreviate ? 'Ctrl' : 'Control';
		case 'alt':
			if(process.platform == 'darwin') { return modifiersAsSymbols ? '\u2325' : 'Option'; }
			else { return 'Alt'; }
		case 'shift':
			return 'Shift';
		case 'meta':
			if(process.platform == 'windows') { return 'Win'; }
			else if(process.platform == 'darwin') { return modifiersAsSymbols ? '\u8984' : (abbreviate ? 'Cmd' : 'Command'); }
			else { return winOnOtherOS ? 'Win' : (modifiersAsSymbols ? '\u25C6' : 'Meta'); }
	}
}


/**
 * Converts from a `keyIdentifier` value to a keyboard event modifier property.
 *
 * @param {string} keyIdentifier - the `keyIdentifier` value
 * @returns {string} the corresponding keyboard event modifier property name
 */
export function modFromKeyIdentifier(keyIdentifier) {
	switch(keyIdentifier) {
		case 'Control': return 'ctrl';
		case 'Alt': return 'alt';
		case 'Shift': return 'shift';
		case 'Win':
		case 'Meta':
			return 'meta';
	}
}


/**
 * Generate a readable string representing the given shortcut definition's key combination.
 *
 * @param {Shortcuts~Definition} shortcut - the shortcut definition to display
 *
 * @returns {string} - the human-readable representation of the given shortcut definition's key combination
 */
export function displayShortcutKeyCombo(shortcut) {
	let key = shortcut.key;
	if(_.includes(keyIdentifiersToUse, key)) {
		key = displayModKey(modFromKeyIdentifier(key));
	} else if(key && key.length == 1) {
		// Capitalize single-letter keys.
		key = key.toUpperCase();
	}

	const modifiers = _(shortcut)
		.pick('ctrl', 'alt', 'shift', 'meta')
		.pickBy() // Remove properties with falsy values.
		.keys()
		.map(displayModKey)
		.value();

	return modifiers.concat(key).join('+');
}


/**
 * A shortcut manager for a given context.
 */
export default class Shortcuts {
	/**
	 * Keys in the config are key names as represented by
	 * [KeyboardEvent.code](https://developer.mozilla.org/en-US/docs/Web/API/KeyboardEvent/code), with the exception of
	 * the modifiers listed in {@linkcode Shortcuts~keyIdentifiersToUse}, which are as represented by
	 * [KeyboardEvent.keyIdentifier](https://developer.mozilla.org/en-US/docs/Web/API/KeyboardEvent/keyIdentifier).
	 *
	 * (this _should_ use [KeyboardEvent.key](https://developer.mozilla.org/en-US/docs/Web/API/KeyboardEvent/key), but
	 * currently Electron does not populate that property)
	 *
	 * @typedef {Object.<string, Array.<Shortcuts~Definition>>} Shortcuts~Config
	 */

	/**
	 * The definition of a keyboard shortcut -- a binding from a key combination to an action in a given context.
	 *
	 * @typedef {object} Shortcuts~Definition
	 *
	 * @property {string} [key] - the
	 * 			[KeyboardEvent.code](https://developer.mozilla.org/en-US/docs/Web/API/KeyboardEvent/code) (or
	 * 			[KeyboardEvent.keyIdentifier](https://developer.mozilla.org/en-US/docs/Web/API/KeyboardEvent/keyIdentifier)
	 * 			for the modifiers listed in {@linkcode Shortcuts~keyIdentifiersToUse}) of the main key for this shortcut
	 *
	 * @property {boolean} [ctrl=false] - if `true`, the "Control" key (or "Command" on Mac OS) _must_ be pressed in
	 * 			order to trigger this shortcut; otherwise, it _must not_ be pressed
	 * @property {boolean} [alt=false] - if `true`, the "Alt" key _must_ be pressed in order to trigger this shortcut;
	 * 			otherwise, it _must not_ be pressed
	 * @property {boolean} [shift=false] - if `true`, the "Shift" key _must_ be pressed in order to trigger this
	 * 			shortcut; otherwise, it _must not_ be pressed
	 * @property {boolean} [meta=false] - if `true`, the "Meta" key _must_ be pressed in order to trigger this shortcut;
	 * 			otherwise, it _must not_ be pressed
	 *
	 * @property {string} action - the name of the function on the attached ViewModel to call when this shortcut is
	 * 			triggered
	 * @property {Array} [args=[]] - an array of arguments to pass to the function identified by `action` when this
	 * 			shortcut is triggered
	 */

	/**
	 * Create a new shortcut manager instance.
	 *
	 * @param vm - the Vue.js ViewModel instance to invoke actions on
	 * @param {string} context - the name of the context of this set of shortcuts; used to look up available actions
	 * 			when configuring, and to get and store shortcut configuration
	 */
	constructor(vm, context) {
		this.vm = vm;
		this.context = context;
	}

	/**
	 * @returns {Shortcuts~Config}
	 */
	config() {
		return getContextShortcuts(this.context);
	}

	handle($event) {
		debugLog('Shortcuts#handle(', $event, ')');
		debugLog('this.config():', this.config());

		let keyName = keyEventCodeRE.exec($event.code)[1];
		if(_.includes(keyIdentifiersToUse, $event.keyIdentifier)) {
			keyName = $event.keyIdentifier;
		}
		debugLog('keyName:', keyName);
		debugLog('this.config()[keyName]:', this.config()[keyName]);

		return _.some(this.config()[keyName], shortcut => {
			if(
				$event.ctrlKey == shortcut.ctrl && $event.altKey == shortcut.alt &&
				$event.shiftKey == shortcut.shift && $event.metaKey == shortcut.meta
			) {
				this.vm[shortcut.action].apply(this.vm, shortcut.args || []);
				$event.preventDefault();
				$event.stopPropagation();
				return true;
			}
		});
	}

	catchInput($event, vmProperty) {
		const shortcut = {
			key: keyEventCodeRE.exec($event.code)[1],
			ctrl: $event.ctrlKey,
			alt: $event.altKey,
			shift: $event.shiftKey,
			meta: $event.metaKey,
		};

		if(_.includes(keyIdentifiersToUse, $event.keyIdentifier)) {
			shortcut.key = $event.keyIdentifier;
		}

		this.vm[vmProperty] = shortcut;
	}
}
