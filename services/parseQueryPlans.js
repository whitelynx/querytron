#!/usr/bin/env node

var _ = require('lodash');

function parseQueryPlans(queryPlans) {
	return queryPlans.map(queryPlan => {
		var nodes = [], links = [];

		function visitNode(plan, parentPlanIdx) {
			var planIdx = nodes.push(_.omit(plan, 'Plans')) - 1;

			if(parentPlanIdx !== undefined) {
				links.push({
					source: planIdx,
					target: parentPlanIdx,
					value: plan["Actual Total Time"],
				});
			}

			if(plan.Plans) {
				plan.Plans.forEach(subPlan => visitNode(subPlan, planIdx));
			}
		}

		visitNode(queryPlan.Plan);

		return { nodes, links };
	});
}

export default parseQueryPlans;
