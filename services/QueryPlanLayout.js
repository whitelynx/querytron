var _ = require('lodash');
import d3 from 'd3';


function center(node) {
	return node.y + node.dy / 2;
}

function value(link) {
	return link.value;
}


export default class QueryPlanLayout {
	constructor() {
		this._nodeWidth = 96;
		this._nodeHeight = 96;
		this._horizontalPadding = 96;
		this._verticalPadding = 32;
		this._nodes = [];
		this._links = [];
		this._maxValue = 0;
		this._maxLinkValue = 0;
		this._bbox = [Infinity, Infinity, -Infinity, -Infinity];
		this._defaultCurvature = 0.5;
	}

	nodeWidth(val) {
		if(!arguments.length) return this._nodeWidth;
		this._nodeWidth = +val;
		return this;
	}

	nodeHeight(val) {
		if(!arguments.length) return this._nodeHeight;
		this._nodeHeight = +val;
		return this;
	}

	horizontalPadding(val) {
		if(!arguments.length) return this._horizontalPadding;
		this._horizontalPadding = +val;
		return this;
	}

	verticalPadding(val) {
		if(!arguments.length) return this._verticalPadding;
		this._verticalPadding = +val;
		return this;
	}

	nodes(val) {
		if(!arguments.length) return this._nodes;
		this._nodes = val;
		return this;
	}

	links(val) {
		if(!arguments.length) return this._links;
		this._links = val;
		return this;
	}

	curvature(val) {
		if(!arguments.length) return this._defaultCurvature;
		this._defaultCurvature = +val;
		return this;
	}

	layout(iterations) {
		this.computeNodeLinks();
		this.computeNodeValues();
		this.computeNodeXPositions();
		this.computeNodeYPositions(iterations);
		this.computeLinkYPositions();
		this.computeBBox();
		return this;
	}

	link() {
		var curvature = this._defaultCurvature;

		function link(d) {
			var x0 = d.source.x + d.source.dx;
			var x1 = d.target.x;
			var xi = d3.interpolateNumber(x0, x1);
			var x2 = xi(curvature);
			var x3 = xi(1 - curvature);
			var y0 = d.source.y + d.sy + d.dy / 2;
			var y1 = d.target.y + d.ty + d.dy / 2;
			return "M" + x0 + "," + y0
			+ "C" + x2 + "," + y0
			+ " " + x3 + "," + y1
			+ " " + x1 + "," + y1;
		}

		link.curvature = function(val) {
			if(!arguments.length) return curvature;
			curvature = +val;
			return link;
		};

		return link;
	}

	bbox() {
		return this._bbox;
	}

	maxValue() {
		return this._maxValue;
	}

	maxLinkValue() {
		return this._maxLinkValue;
	}

	computeBBox() {
		this._bbox = [Infinity, Infinity, -Infinity, -Infinity];
		this._nodes.forEach(node => {
			this._bbox = [
				Math.min(this._bbox[0], node.x),
				Math.min(this._bbox[1], node.y),
				Math.max(this._bbox[2], node.x + node.dx),
				Math.max(this._bbox[3], node.y + node.dy),
			];
		});
	}

	// Populate the sourceLinks and targetLinks for each node.
	// Also, if the source and target are not objects, assume they are indices.
	computeNodeLinks() {
		this._nodes.forEach((node) => {
			node.sourceLinks = [];
			node.targetLinks = [];
		});
		this._links.forEach(link => {
			var { source, target } = link;
			if(typeof source === "number") source = link.source = this._nodes[link.source];
			if(typeof target === "number") target = link.target = this._nodes[link.target];
			source.sourceLinks.push(link);
			target.targetLinks.push(link);
		});
	}

	// Compute the value of each node by summing the associated links.
	computeNodeValues() {
		this._nodes.forEach(node => {
			node.value = Math.max(
				d3.sum(node.sourceLinks, value),
				d3.sum(node.targetLinks, value)
			);
		});
	}

	// Iteratively assign the X position of each node.
	// Nodes are assigned the maximum X position of incoming neighbors plus one;
	// nodes with no incoming links are assigned X position zero.
	computeNodeXPositions() {
		var remainingNodes = this._nodes;
		var nextNodes;
		var x = 0;

		while(remainingNodes.length) {
			nextNodes = [];
			remainingNodes.forEach(node => {
				node.x = x;
				node.dx = this._nodeWidth;
				node.sourceLinks.forEach(link => {
					nextNodes.push(link.target);
				});
			});
			remainingNodes = nextNodes;
			x += this._nodeWidth + this._horizontalPadding;
		}

		this.moveSourcesRight();
	}

	moveSourcesRight() {
		_(this._nodes)
			.orderBy(['x'], ['desc'])
			.forEach(node => {
				if(node.sourceLinks.length > 0) {
					node.x = d3.min(node.sourceLinks, d => d.target.x) -
						this._nodeWidth - this._horizontalPadding;
				}
			});
	}

	computeNodeYPositions(iterations) {
		var nodesByX = d3.nest()
			.key(d => d.x)
			.sortKeys(d3.ascending)
			.entries(this._nodes)
			.map(d => d.values);

		const initializeNodeYPosition = () => {
			this._maxValue = d3.max(nodesByX, nodesInRank => d3.max(nodesInRank, value));
			this._maxLinkValue = d3.max(this._links, value);

			nodesByX.forEach(nodesInRank => {
				nodesInRank.forEach((node, i) =>{
					node.y = i;
					node.dy = this._nodeHeight;
				});
			});

			this._links.forEach(link => {
				link.dy = link.value / this._maxValue * this._nodeHeight;
			});
		};

		const relaxLeftToRight = (alpha) => {
			nodesByX.forEach(nodesInRank => {
				nodesInRank.forEach(node => {
					if(node.targetLinks.length) {
						var y = d3.sum(node.targetLinks, weightedSource) / d3.sum(node.targetLinks, value);
						node.y += (y - center(node)) * alpha;
					}
				});
			});

			function weightedSource(link) {
				return center(link.source) * link.value;
			}
		};

		const relaxRightToLeft = (alpha) => {
			nodesByX.slice().reverse().forEach(nodesInRank => {
				nodesInRank.forEach(node => {
					if(node.sourceLinks.length) {
						var y = d3.sum(node.sourceLinks, weightedTarget) / d3.sum(node.sourceLinks, value);
						node.y += (y - center(node)) * alpha;
					}
				});
			});

			function weightedTarget(link) {
				return center(link.target) * link.value;
			}
		};

		const resolveCollisions = () => {
			var lastWeightedAvgY, minY = Infinity;
			nodesByX.forEach(nodesInRank => {
				var node, i;
				var y0 = 0;
				var n = nodesInRank.length;
				var totalValue = 0, weightedSumY = 0;
				var totalValueWithTargets = 0, weightedSumYWithTargets = 0;

				// Push any overlapping nodes down.
				nodesInRank.sort(ascendingY);
				for(i = 0; i < n; ++i) {
					node = nodesInRank[i];
					node.y = y0;

					minY = Math.min(minY, node.y);

					if(lastWeightedAvgY === undefined) {
						totalValue += node.value;
						weightedSumY += (node.y + this._nodeHeight / 2) * node.value;
					}

					if(node.targetLinks.length > 0) {
						totalValueWithTargets += node.value;
						weightedSumYWithTargets += (node.y + this._nodeHeight / 2) * node.value;
					}

					y0 = node.y + node.dy + this._verticalPadding;
				}

				if(lastWeightedAvgY !== undefined) {
					var weightedAvgYWithTargets = weightedSumYWithTargets / (totalValueWithTargets || 1);
					for(i = 0; i < n; ++i) {
						node = nodesInRank[i];

						node.y += lastWeightedAvgY - weightedAvgYWithTargets;

						minY = Math.min(minY, node.y);

						totalValue += node.value;
						weightedSumY += (node.y + this._nodeHeight / 2) * node.value;
					}
				}

				lastWeightedAvgY = weightedSumY / totalValue;
			});

			// Shift all nodes so the topmost node is at y=0.
			var node;
			for(let i = 0; i < this._nodes.length; ++i) {
				node = this._nodes[i];
				node.y -= minY;
			}
		};

		initializeNodeYPosition();
		resolveCollisions();
		for(let alpha = 1; iterations > 0; --iterations) {
			alpha *= 0.99;
			relaxRightToLeft(alpha);
			resolveCollisions();
			relaxLeftToRight(alpha);
			resolveCollisions();
		}

		function ascendingY(a, b) {
			return a.y - b.y;
		}
	}

	computeLinkYPositions() {
		this._nodes.forEach(node => {
			node.sourceLinks.sort(ascendingTargetY);
			node.targetLinks.sort(ascendingSourceY);
		});
		this._nodes.forEach(node => {
			var sy = (1 - d3.sum(node.sourceLinks, value) / this._maxValue) * this._nodeHeight / 2,
				ty = (1 - d3.sum(node.targetLinks, value) / this._maxValue) * this._nodeHeight / 2;
			node.sourceLinks.forEach(link => {
				link.sy = sy;
				sy += link.dy;
			});
			node.targetLinks.forEach(link => {
				link.ty = ty;
				ty += link.dy;
			});
		});

		function ascendingSourceY(a, b) {
			return a.source.y - b.source.y;
		}

		function ascendingTargetY(a, b) {
			return a.target.y - b.target.y;
		}
	}
}
