import fs from 'fs';
import path from 'path';

import _ from 'lodash';
import Promise from 'bluebird';
import mkdirp from 'mkdirp';
import xdg from 'xdg-env';

Promise.promisifyAll(fs);
const mkdirpAsync = Promise.promisify(mkdirp);


function loadFirstConfigFile(configFilenames) {
	return _(configFilenames)
		.map((configFile) => {
			try {
				return JSON.parse(fs.readFileSync(configFile));
			} catch(exc) {
				if(exc.code == 'ENOENT') {
					// Ignore when the file doesn't exist.
					console.log("Config file %s doesn't exist; skipping.", configFile);
				} else {
					console.error('Error reading config file %s:', configFile, exc);
				} // end if
			} // end try
		})
		.filter()
		.first();
}


const configFiles = _([xdg.CONFIG_HOME].concat(xdg.CONFIG_DIRS))
	.map((dir) => path.join(dir, 'querytron', 'config.json'))
	.value();


export const defaultConfig = loadFirstConfigFile([
	path.join(path.dirname(path.dirname(__dirname)), 'config', 'default.json'),
	path.join(path.dirname(__dirname), 'config', 'default.json'),
]);

export const config = _.defaults(
	loadFirstConfigFile(configFiles),
	defaultConfig
);


export function save() {
    return mkdirpAsync(path.dirname(configFiles[0]))
        .then(() => fs.writeFileAsync(configFiles[0], JSON.stringify(config, null, 4)));
}
