import _ from 'lodash';


const messages = [];

const defaultIcons = {
	danger: 'exclamation-circle',
	warning: 'exclamation-triangle',
	//primary: 'exclamation-triangle',
	//success: 'exclamation-triangle',
};

class Message {
	constructor(props) {
		this.time = Date.now();
		this.dismissed = false;
		this.icon = defaultIcons[props.status];
		_.assign(this, props);
		if(this.timeout) {
			setTimeout(() => this.dismiss(), this.timeout);
		}
	}

	dismiss() {
		this.dismissed = true;
	}
}

export function addMessage(message) {
	const msg = new Message(message);
	messages.push(msg);
	return msg;
}

export function activeMessages(msgs) {
	return msgs.filter(msg => !msg.dismissed);
}

export default {
	messages,
	addMessage,
	activeMessages
};
