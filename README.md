querytron
=========

A simple multi-database query tool built on [Electron][] and [Vue.js][].

[Electron]: http://electron.atom.io/
[Vue.js]: http://vuejs.org/

![Query Plan view](screenshots/query-plan-view.png)

QueryTron sports several interesting features:
- A pretty, easy-to-use query plan viewer
- First-class support for geometry types (plotting them on a map right in the results view)
- The ability to "pin" multiple result sets and switch between them
- A pretty in-depth database object browser


Building
--------

* clone repo
* `nvm use`
* `nvm install`
* `npm install`
* `npm run build`


Running
-------

* `npm start`
