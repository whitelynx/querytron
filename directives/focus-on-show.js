import Vue from 'vue';

Vue.directive('focus-on-show', {
	update() {
		this.vm.$on('shown', () => { this.el.focus(); return true; });
	},
});
