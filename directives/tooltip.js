import _ from 'lodash';
import Vue from 'vue';

Vue.directive('tooltip', {
	deep: true,
	priority: 500,
	params: ['title'],
	bind() {
		var options = {
			delay: 100,
		};
		if(this.params.title) {
			options.src = this.params.title;
		}

		this.tooltip = UIkit.tooltip(UIkit.$(this.el), options);
	},
	update(val) {
		if(_.isUndefined(val)) { return; }

		if(_.isPlainObject(val)) {
			var tooltipText = val.src || val.text;
			if(!_.isUndefined(tooltipText)) {
				val.src = tooltipText;
			}

			for(let key in val) {
				this.tooltip.options[key] = val[key];
			}
		} else {
			this.tooltip.options.src = val;
		}

		if(this.tooltip.options.autoShow) {
			this.tooltip.show();
		}
	},
    paramWatchers: {
        title(val, oldVal) {
			this.update(val, oldVal);
        }
    }
});
