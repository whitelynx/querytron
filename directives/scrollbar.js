import _ from 'lodash';
import Vue from 'vue';
import Ps from 'perfect-scrollbar';


const scrollbarModule = {
    defaultOptions: {
		minScrollbarLength: 16,
    },

    initialize(element, options) {
        Ps.initialize(element, _.defaults({}, options, scrollbarModule.defaultOptions));

        return {
            get element() {
                return element;
            },
            update() {
                Ps.update(element);
            },
            scrollTo(position) {
                element.scrollTop = position;
                Ps.update(element);
            }
        };
    }
};

Vue.directive('scrollbar', {
    update(options) {
        var scrollbar = scrollbarModule.initialize(this.el, options);

        this.vm.$scrollTo = scrollbar.scrollTo;
        this.vm.$updateScrollbar = scrollbar.update;

        this.vm.$on('resize', () => { scrollbar.update(); return true; });
        this.vm.$nextTick(() => scrollbar.update());
    },
    unbind() {
        Ps.destroy(this.el);
    }
});

export default scrollbarModule;
