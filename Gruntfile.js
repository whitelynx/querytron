module.exports = function(grunt)
{
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        project: {
            components: 'components/**/*.vue',
            pages: 'pages/**/*.vue',
            services: 'services/**/*.js',
			queries: 'services/**/*.sql',
            directives: 'directives/**/*.js',
            images: 'node_modules/leaflet/dist/images/*',
            dist: 'dist/',
        },
        clean: ["<%= project.dist %>"],
        copy: {
			index: {
				src: 'index.html',
				dest: '<%= project.dist %>index.html',
			},
            queries: {
                src: '<%= project.queries %>',
                dest: '<%= project.dist %>',
            },
            images: {
                expand: true,
                flatten: true,
                src: '<%= project.images %>',
                dest: '<%= project.dist %>images/',
            },
        },
        babel: {
            options: {
                sourceMap: true,
            },
            services: {
				expand: true,
				src: ['<%= project.services %>', '<%= project.directives %>', 'app.js'],
				dest: '<%= project.dist %>',
            },
        },
        less: {
            dark: {
                options: {
                    //compress: true,
                    paths: ['node_modules'],
                },
                files: {
                    '<%= project.dist %>css/dark.css': 'themes/dark/theme.less',
                },
            },
            light: {
                options: {
                    //compress: true,
                    paths: ['node_modules'],
                },
                files: {
                    '<%= project.dist %>css/light.css': 'themes/light/theme.less',
                },
            },
        },
        vueify: {
            components: {
                files: [
                    {
                        expand: true,
                        src: '<%= project.components %>',
                        dest: '<%= project.dist %>',
                        ext: '.vue.js',
                    },
                ],
            },
            pages: {
                files: [
                    {
                        expand: true,
                        src: '<%= project.pages %>',
                        dest: '<%= project.dist %>',
                        ext: '.vue.js',
                    },
                ],
            },
        },
        eslint: {
            all: ['**/*.js', '**/*.vue', '!dist/**', '!node_modules/**'],
        },
    });

    grunt.loadNpmTasks('grunt-babel');
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-less');
    grunt.loadNpmTasks('grunt-vueify');
    grunt.loadNpmTasks('gruntify-eslint');

    grunt.registerTask('build-no-check', ['clean', 'copy', 'babel', 'vueify', 'less']);
    grunt.registerTask('build', ['eslint', 'build-no-check']);
    grunt.registerTask('default', ['build']);
};
