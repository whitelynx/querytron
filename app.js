import electron from 'electron';
import Vue from 'vue';
import VueValidator from 'vue-validator';

import ConnectionBar from './components/ConnectionBar.vue';
import GlobalSettings from './components/GlobalSettings.vue';

import BrowsePage from './pages/Browse.vue';
import QueryPage from './pages/Query.vue';

import connectionService from './services/connection';
import messages from './services/messages';

Vue.config.debug = true;

import './directives/focus-on-show';
import './directives/scrollbar';
import './directives/tooltip';

import './services/keyCodes';

Vue.use(VueValidator);

import { config } from './services/config';

const appVue = new Vue({
	el: 'html',
    data: {
        connectionService,
        config,
		currentPage: 'BrowsePage',
		messages
    },
    components: {
        ConnectionBar,
        GlobalSettings,
		BrowsePage,
		QueryPage
    },
	ready() {
		this.$watch('currentPage', () => {
			this.$nextTick(() => {
				this.$broadcast('resize');
			});
		});

	}
});

electron.ipcRenderer.on('openFile', function(event, filePath) {
	appVue.currentPage = 'QueryPage';
	Vue.nextTick(() => {
		appVue.$broadcast('query.openFile', filePath);
	});
});

electron.ipcRenderer.on('saveFile', function(event, filePath) {
	appVue.currentPage = 'QueryPage';
	Vue.nextTick(() => {
		appVue.$broadcast('query.saveFile', filePath);
	});
});

electron.ipcRenderer.on('saveResultsFile', function(event, filePath) {
	appVue.currentPage = 'QueryPage';
	Vue.nextTick(() => {
		appVue.$broadcast('query.saveResultsFile', filePath);
	});
});
