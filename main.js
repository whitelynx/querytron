import electron from 'electron';

import minimist from 'minimist';

import './services/appMenu';


var argv = minimist(process.argv.slice(2));

// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
var mainWindow = null;

// Quit when all windows are closed.
electron.app.on('window-all-closed', () => {
    // On OS X it is common for applications and their menu bar
    // to stay active until the user quits explicitly with Cmd+Q.
    if(process.platform != 'darwin')
    {
        electron.app.quit();
    } // end if
});

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
electron.app.on('ready', () => {
    // Create the browser window.
    mainWindow = new electron.BrowserWindow({
        width: 1280, height: 800,
        darkTheme: true,
        title: 'QueryTron',
		webPreferences: {
			nodeIntegration: true
		},
    });

    // and load the index.html of the app.
    mainWindow.loadURL('file://' + __dirname + '/dist/index.html');

	switch((process.env.SHOW_DEVTOOLS || '').toLowerCase()) {
		case '':
		case '0':
		case 'off':
		case 'no':
		case 'f':
		case 'false':
			break;
		default:
			// Load the Vue.js dev tools extension.
			//mainWindow.addDevToolsExtension('/home/dbronke/.config/chromium/Default/Extensions/nhdogjmejiglipccpnnnanhbledajbpd/1.1.6_0');

			// Open the DevTools.
			mainWindow.openDevTools();
	}

    console.log("mainWindow.id: %j", mainWindow.id);

	mainWindow.webContents.on('did-finish-load', function() {
		if(argv._.length > 0) {
			argv._.forEach((filePath) => {
				mainWindow.webContents.send('openFile', filePath);
			});
		}
	});

	electron.app.on('open-file', (event, filePath) => {
		event.preventDefault();
		mainWindow.webContents.send('openFile', filePath);
	});

	mainWindow.webContents.on('devtools-opened', () => {
		mainWindow.webContents.addWorkSpace(__dirname);
	});

	const queryFileDialogFilters = [
		{ name: 'SQL Query', extensions: ['sql', 'pgsql', 'tsql'] },
		{ name: 'All Files', extensions: ['*'] },
	];

	const resultsFileDialogFilters = [
		{ name: 'CSV File', extensions: ['csv'] },
		{ name: 'All Files', extensions: ['*'] },
	];

	electron.ipcMain.on('showOpenDialog', function() {
		console.log("Showing `Open` dialog...");
		var dialogOptions = {
			title: 'Open Query',
			properties: ['openFile'],
			filters: queryFileDialogFilters,
		};

		electron.dialog.showOpenDialog(mainWindow, dialogOptions, filenames => {
			if(filenames) {
				filenames.forEach((filePath) => {
					mainWindow.webContents.send('openFile', filePath);
				});
			}
		});
	});

	electron.ipcMain.on('showSaveAsDialog', function() {
		console.log("Showing `Save Query As` dialog...");
		var dialogOptions = {
			title: 'Save Query As',
			filters: queryFileDialogFilters,
		};

		electron.dialog.showSaveDialog(mainWindow, dialogOptions, filePath => {
			if(filePath) {
				mainWindow.webContents.send('saveFile', filePath);
			}
		});
	});

	electron.ipcMain.on('showSaveResultsAsDialog', function() {
		console.log("Showing `Save Results As` dialog...");
		var dialogOptions = {
			title: 'Save Results As',
			filters: resultsFileDialogFilters,
		};

		electron.dialog.showSaveDialog(mainWindow, dialogOptions, filePath => {
			if(filePath) {
				mainWindow.webContents.send('saveResultsFile', filePath);
			}
		});
	});

    // Emitted when the window is closed.
    mainWindow.on('closed', () => {
        // Dereference the window object, usually you would store windows
        // in an array if your app supports multi windows, this is the time
        // when you should delete the corresponding element.
        mainWindow = null;
    });
});
