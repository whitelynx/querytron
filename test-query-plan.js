//
// Prerequisites:
//    npm install -g browserify
//    npm install babelify babel-preset-es2015 babel-runtime babel-plugin-transform-runtime
//
// To build:
//    browserify test-query-plan.js -t babelify -o test-query-plan-bundle.js
//

import d3 from 'd3';

import parseQueryPlans from './services/parseQueryPlans';
import QueryPlanLayout from './services/QueryPlanLayout';

// PostgreSQL query plan viewer (based loosely on d3.sankey)

// Configurable params
var config = {
	nameKey: 'Node Type',
	valueKey: 'Actual Total Time',
	horizontalMargin: 20,
	verticalMargin: 28,
	//layoutRelaxIterations: 32,
	layoutRelaxIterations: 0,
	nodeSize: 64,
	horizontalPadding: 96,
	verticalPadding: 48,
	cornerRadius: 8,
	cteColor: d3.rgb(0, 255, 255),
	minScale: 0.1,
	maxScale: 2,
};

//var queryPlan = parseQueryPlans(require('./test-query-plan.json'))[0];
var queryPlan = parseQueryPlans(require('./test-query-plan-2.json'))[0];

var _nextID = 0;
function getID() {
	return 'id-' + _nextID++;
}

function assignID(data) {
	data.nodes.forEach(function (d) {
		d.id = getID();
	});
	data.links.forEach(function (d) {
		d.id = getID();
	});
	return data;
}

console.log(queryPlan);

setImmediate(() => visualize(assignID(queryPlan)));


var iconBasePath = 'node_modules/basic-query-plan-icons';
var fallbackNodeIcon = `${iconBasePath}/background.svg`;
var nodeTypeIcons = {
	'Aggregate': `${iconBasePath}/Aggregate.svg`,
	'Append': `${iconBasePath}/Append.svg`,
	'Bitmap Heap Scan': `${iconBasePath}/Bitmap Heap Scan.svg`,
	'Bitmap Index Scan': `${iconBasePath}/Bitmap Index Scan.svg`,
	'Function Scan': `${iconBasePath}/Function Scan.svg`,
	'Hash Anti Join': `${iconBasePath}/Hash Anti Join.svg`,
	'Hash Join': `${iconBasePath}/Hash Join.svg`,
	'Hash Setop Except': `${iconBasePath}/Hash Setop Except.svg`,
	'Hash Setop Intersect': `${iconBasePath}/Hash Setop Intersect.svg`,
	'Hash Setop Unknown': `${iconBasePath}/Hash Setop Unknown.svg`,
	'Hash': `${iconBasePath}/Hash.svg`,
	'Index Only Scan': `${iconBasePath}/Index Only Scan.svg`,
	'Index Scan': `${iconBasePath}/Index Scan.svg`,
	'Limit': `${iconBasePath}/Limit.svg`,
	'Materialize': `${iconBasePath}/Materialize.svg`,
	'Nested Loop': `${iconBasePath}/Nested Loop.svg`,
	'Result': `${iconBasePath}/Result.svg`,
	'Seq Scan': `${iconBasePath}/Seq Scan.svg`,
	'Sort': `${iconBasePath}/Sort.svg`,
	'Window Agg': `${iconBasePath}/Window Agg.svg`,
};

const cteSubplanRE = /^CTE /;


function visualize(data) {

	var svg = d3.select('svg')
		.on('dblclick.zoom', null);

	var defs = svg.append('defs');

	var dropShadowFilter = defs.append('filter')
		.attr('id', 'dropshadow');

	dropShadowFilter.append('feOffset')
		.attr('result', 'offsetOut')
		.attr('in', 'SourceAlpha');

	dropShadowFilter.append('feGaussianBlur')
		.attr('result', 'blurOut')
		.attr('in', 'offsetOut')
		.attr('stdDeviation', 2);

	dropShadowFilter.append('feComponentTransfer')
		.attr('result', 'fadeOut')
		.attr('in', 'blurOut')
		.call(ct => {
			ct.append('feFuncR')
				.attr('type', 'linear')
				.attr('slope', 1).attr('intercept', 0);
			ct.append('feFuncG')
				.attr('type', 'linear')
				.attr('slope', 1).attr('intercept', 0);
			ct.append('feFuncB')
				.attr('type', 'linear')
				.attr('slope', 1).attr('intercept', 0);
			ct.append('feFuncA')
				.attr('type', 'linear')
				.attr('slope', 0.7).attr('intercept', 0);
		});

	dropShadowFilter.append('feBlend')
		.attr('in', 'SourceGraphic')
		.attr('in2', 'fadeOut')
		.attr('mode', 'normal');

	var width = parseInt(svg.style('width'), 10);
	var height = parseInt(svg.style('height'), 10);
	console.log(width, height);

	var gRoot = svg.append('svg:g')
		.attr('transform', `translate(${config.horizontalMargin}, ${config.verticalMargin})`);

	var zoom = d3.behavior.zoom()
		.scaleExtent([config.minScale, config.maxScale])
		.size([width, height])
		.translate([config.horizontalMargin, config.verticalMargin])
		.on("zoom", function() {
			gRoot
				.interrupt('zoom')
				.transition('zoom')
				.duration(10)
				.ease('easeOutQuart')
				.attr('transform', `translate(${d3.event.translate}) scale(${d3.event.scale})`);
			});

	svg.call(zoom);

	// Set the query plan diagram properties
	var queryPlanDiagram = new QueryPlanLayout()
		.nodeWidth(config.nodeSize)
		.nodeHeight(config.nodeSize - 2 * config.cornerRadius)
		.horizontalPadding(config.horizontalPadding)
		.verticalPadding(config.verticalPadding)
		.curvature(0.6)
		.nodes(data.nodes)
		.links(data.links)
		.layout(config.layoutRelaxIterations);

	function fitToViewport() {
		var bbox = queryPlanDiagram.bbox();
		var scale = Math.max(
			Math.min(
				(width - 2 * config.horizontalMargin) / (bbox[2] - bbox[0]),
				(height - 2 * config.verticalMargin) / (bbox[3] - bbox[1]),
				config.maxScale
			),
			config.minScale
		);
		zoom.scale(scale).event(svg);
	}
	setImmediate(fitToViewport);
	window.onresize = () => {
		width = parseInt(svg.style('width'), 10);
		height = parseInt(svg.style('height'), 10);
		fitToViewport();
	};

	console.log('queryPlanDiagram.maxLinkValue():', queryPlanDiagram.maxLinkValue());
	var color = d3.scale.linear()
	//var color = d3.scale.log()
		.domain([0, queryPlanDiagram.maxLinkValue() * 0.5, queryPlanDiagram.maxLinkValue()])
		.range([d3.rgb(0, 191, 0), d3.rgb(255, 191, 0), d3.rgb(255, 0, 0)]);

	var path = queryPlanDiagram.link(); // path function

	var link = gRoot.selectAll(".link")
		.data(data.links)
		.enter().append("path")
		.attr("class", "link")
		.attr('id', d => d.id)
		.attr("d", path)
		.style('stroke', d => color(d.source[config.valueKey]))
		.style("stroke-width", d => Math.max(1, d.dy))
		.sort((a, b) => b.dy - a.dy);

	link.append("title")
		.text(d => `${d.source[config.nameKey]} → ${d.target[config.nameKey]}\n${d.source[config.valueKey]}`);

	var node = gRoot.selectAll(".node")
		.data(data.nodes)
		.enter().append("g")
		.attr("class", "node")
		.attr("transform", d => `translate(${d.x},${d.y - config.cornerRadius})`)
		.each(d => d.icon = nodeTypeIcons[d['Node Type']]);

	var nodeWithoutIcon = node.filter(d => !d.icon)
		.each(d => d.icon = fallbackNodeIcon);

	node.append("image")
		.attr("width", config.nodeSize)
		.attr("height", config.nodeSize)
		.attr("xlink:href", d => d.icon)
		.attr("filter", 'url(#dropshadow)')
		.append("title")
		.text(d => `${d[config.nameKey]}\n${d[config.valueKey]}`);

	nodeWithoutIcon.append("text")
		.attr("x", config.nodeSize / 2)
		.attr("y", config.nodeSize / 2)
		.attr("dominant-baseline", "central")
		.attr("text-anchor", "middle")
		.attr("textLength", config.nodeSize - config.cornerRadius) // Only half a corner radius padding on each side.
		.attr("transform", null)
		.text(d => d[config.nameKey]);

	node.filter(d => d['Relation Name'] || d['CTE Name'])
		.append("text")
			.attr('class', 'small')
			.attr("x", config.nodeSize / 2)
			.attr("y", 0)
			.attr("dominant-baseline", "text-after-edge")
			.attr("text-anchor", "middle")
			.attr("transform", null)
			.text(d => d['Relation Name'] || d['CTE Name'])

			.filter(d => !d['Relation Name'])
				.style("fill", config.cteColor);

	node.filter(d => d['Subplan Name'])
		.append("text")
			.attr('class', 'small')
			.attr("x", config.nodeSize / 2)
			.attr("y", config.nodeSize)
			.attr("dominant-baseline", "text-before-edge")
			.attr("text-anchor", "middle")
			.attr("transform", null)
			.text(d => d['Subplan Name'].replace(cteSubplanRE, ''))

			.filter(d => cteSubplanRE.test(d['Subplan Name']))
				.style("fill", config.cteColor);

	node.filter(d => d['Node Type'] == 'Aggregate' && d['Strategy'])
		.append("text")
			.attr('class', 'overlay')
			.attr("x", config.nodeSize / 2)
			.attr("y", config.nodeSize - config.cornerRadius)
			.attr("dominant-baseline", "text-after-edge")
			.attr("text-anchor", "middle")
			.attr("transform", null)
			.text(d => d['Strategy']);

	node.on('mouseover', function (d) {
		console.log(d);
		d3.selectAll('.link').classed('active', false);
		d.sourceLinks.forEach(function (x) {
			d3.select('#' + x.id).classed('active', true);
		});
		d.targetLinks.forEach(function (x) {
			d3.select('#' + x.id).classed('active', true);
		});
	});
	node.on('mouseout', function () {
		d3.selectAll('.link').classed('active', false);
	});
}

d3.selection.prototype.moveToFront = function() {
	return this.each(function() {
		this.parentNode.appendChild(this);
	});
};
